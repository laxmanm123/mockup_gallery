var base_url = $("#base_url").val();
$("#submit").hide();
$("#fake").show();

$("#email").keyup(function(){
	var email = $("#email").val();

	$.ajax({
		'type':'POST',
		'url': base_url+'admin_home/check_email',
		'data':{'email':email},
		'dataType':'text',
		'cache':false,
		'success':function(data){
			if (data == 1) {
				$("#err").html('');
				$("#submit").show();
				$("#fake").hide();
			}else {
				$("#err").html('<span class="text-danger">Email Already in use..!!</span>');
				$("#submit").hide();
				$("#fake").show();
			}
		}
	});
});