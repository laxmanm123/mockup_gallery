$("body").on("click",".show-modal",function(){
	var id = $(this).data("id");
  var searchKeyword = $(this).data("searchKeyword");
	$.ajax({
	 	url: base_url+"home/ajaxPageModal",
        dataType:"html", 
	 	data: {id:id,searchKeyword:searchKeyword},
	 	success: function(result){
            $("#pageModal .modal-content").empty();
            $("#pageModal .modal-content").html(result);
            $("#pageModal").modal('show'); 
            initModalContent();
        }
    });
});

function redirect(url) {
  if (confirm('Are you sure you want to delete your mockup?')) {
    window.location.href=url;
  }
  return false;
}

// Comment Reply 
$('body').on('show.bs.modal', '#reply-msg-popup', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var dataURL = button.attr('href') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this);
  modal.find('.modal-content').load(dataURL,function(){
            modal.modal({show:true});
            initReplyModal();

  });
  //initReplyModal();
});

$('#reply-msg-popup').on('hidden.bs.modal', function (e) {
    $(e.target).removeData('bs.modal').find(".modal-content").html('');
});
$('#reply-msg-popup').on('loaded.bs.modal', function (e) {
    initReplyModal();
});
function initModalContent(){
    var clipboardBtn = document.getElementById('clipboard-btn');
    var clipboard = new ClipboardJS(clipboardBtn);
    clipboard.on('success', function(e) {
        //console.log(e);
    });
    clipboard.on('error', function(e) {
        //console.log(e);
    });

    var clipboardBtn2 = document.getElementById('clipboard-btn2');
    var clipboard2 = new ClipboardJS(clipboardBtn2);
    clipboard2.on('success', function(e) {
        //console.log(e);
    });
    clipboard2.on('error', function(e) {
        //console.log(e);
    });
}

function initReplyModal(){
    $('#reply-msg-form').on('submit', function (e) {
        e.preventDefault();
        $("#reply-msg-save").prop("disabled", true);
        $('#reply-msg-error').text('Please wait... We are submitting!');
        
        var frm = $('#reply-msg-form');
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: 'json',
            success: function (d) {
                $("#reply-msg-save").prop("disabled", false);
                $('#reply-msg-error').text('');
                $('textarea[name="msg"]').val('');
                $('#row_'+d.id).find('.reply-msg-btn').removeClass('reply-count').addClass('reply-count');
                $('#reply-modal-content').load($('#modal-url').val(), function() {
                    initReplyModal();
                });
            }
        });
    });
    $('.reply-msg').unbind('click').bind('click', function (e) {
        e.preventDefault();
        var reply_id =$(this).data("id");
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: $(this).attr("href"),
                type: "POST",
                data: {},
                dataType: 'json',
                success: function (d) {
                    $('#reply_'+reply_id).hide();
                    if(d.discussion_count > 0){
                        $('#row_'+d.mockup_id).find('.reply-msg-btn').removeClass('reply-count').addClass('reply-count');
                    }else{
                        $('#row_'+d.mockup_id).find('.reply-msg-btn').removeClass('reply-count');
                    }
                }
            });
        }
    });
}

$("#vertical-menu h3").click(function () {
    //Inner 
    var jqInner = $(this).next();
    if (jqInner.is(":visible"))
    {
        jqInner.slideUp()
        $(this).find('span').html('-');
    }
    else
    {
        jqInner.slideDown()
        $(this).find('span').html('+');
    }
})