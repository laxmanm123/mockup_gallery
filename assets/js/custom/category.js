var base_url = $("#base_url").val();
$("#submit").hide();
$("#fake").show();

// Profile pic Upload jQuery Code
$("#icon").change(function(){
    var img = document.getElementById('icon');

    readIMG(this);
    
    var _URL = window.URL || window.webkitURL;
    var image, logo;
    if ((logo = this.files[0])) {
        image = new Image();
        image.onload = function() {
            var width = this.width;
            var height = this.height;

            if(width == 62 && height == 62){
                $("#res").html("");
                $("#submit").show();
                $("#fake").hide();
            }else {
                $("#res").html("Uploaded image size is incorrect..!!");
                $("#submit").hide();
                $("#fake").show();
            }
        };
        image.src = _URL.createObjectURL(logo);
    }
});

$("#icon").change(function(){
    var file = $("#icon").val();
    var exe = this.value.match(/\.(.+)$/)[1];

    if(file != '' && exe == 'png'){
        $("#submit").show();
        $("#fake").hide();
    }else {
        $("#res").html("This File type is not allowed. Only JPG, JPEG & PNG allowed");
        $("#submit").hide();
        $("#fake").show();
    }
});

$("#type").change(function(){
    var type = $("#type").val();
    if (type == 1) {
        $("#link").prop('disabled', false);
    }else {
        $("#link").prop('disabled', true);
    }
});

// Profile pic upload JS Code
function readIMG(input){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
             $('#img_upload').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }else{
         $('#img_upload').attr('src', base_url+'assets/images/no-img.png');
         $("#res").html("");
    }
}

$('.category-del').unbind('click').bind('click', function (e) {
    e.preventDefault();
    var index =$(this).data("id");
    if (confirm('Are you sure you want to delete this?')) {
        $.ajax({
            url: $(this).attr("href"),
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {
                $('#cat_'+index).slideUp();
            }
        });
    }
});