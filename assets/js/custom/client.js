$("#submit").hide();
$("#logo").change(function(){
    var img = document.getElementById('logo');

    readIMG(this);
    
    var _URL = window.URL || window.webkitURL;
    var image, logo;
    if ((logo = this.files[0])) {
        image = new Image();
        image.onload = function() {
            var width = this.width;
            var height = this.height;

            if(width <= 200 && height <= 200){
                $("#res").html("");
                $("#submit").show();
                $("#fake").hide();
            }else {
                $("#res").html("Uploaded image size is incorrect..!!");
                $("#submit").hide();
                $("#fake").show();
            }
        };
        image.src = _URL.createObjectURL(logo);
    }
});

$("#logo").change(function(){
    var file = $("#logo").val();
    var exe = this.value.match(/\.(.+)$/)[1];

    if(file != '' && exe == 'png'){
        $("#submit").show();
        $("#fake").hide();
    }else {
        $("#res").html("This File type is not allowed. Only PNG allowed");
        $("#submit").hide();
        $("#fake").show();
    }
});

function readIMG(input){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
             $('#img_upload').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }else{
         $('#img_upload').attr('src', base_url+'assets/images/no-img.png');
         $("#res").html("");
    }
}