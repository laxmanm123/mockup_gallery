var solution = [];
var tool = [];
var subject = [];
var brandonloads = $('#multiselect1 option:selected');
        $(brandonloads).each(function(index, brand){
            solution.push([$(this).val()]);
        });
var brandonloads = $('#multiselect2 option:selected');
        $(brandonloads).each(function(index, brand){
            tool.push([$(this).val()]);
        });
var brandonloads = $('#multiselect3 option:selected');
        $(brandonloads).each(function(index, brand){
            subject.push([$(this).val()]);
        });
$(function() {
    $('#multiselect1').multiselect({
        includeSelectAllOption: true,
        templates: {
            li: '<li><a class="dropdown-item"><label class="m-0 pl-2 pr-0"></label></a></li>',
            ul: ' <ul class="multiselect-container dropdown-menu p-1 m-0"></ul>',
            button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown" data-flip="false"><span class="multiselect-selected-text"></span> <b class="caret"></b></button>',
            filter: '<li class="multiselect-item filter"><div class="input-group m-0"><input class="form-control multiselect-search" type="text"></div></li>',
            filterClearBtn: '<span class="input-group-btn"><button class="btn btn-secondary multiselect-clear-filter" type="button"><i class="fas fa-minus-circle"></i></button></span>'
        },
        buttonContainer: '<div class="dropdown" />',
        buttonClass: 'btn btn-secondary',
        
        enableFiltering: true,
        buttonWidth: '100%',
        maxHeight: 400,
        nonSelectedText: 'Solution Tag',
        onChange: function(element, checked) {
            solution = [];
            var brands = $('#multiselect1 option:selected');
            $(brands).each(function(index, brand){
                solution.push([$(this).text().trim()]);
            });
            console.log(solution);
        }
    });
    $('#multiselect2').multiselect({
        includeSelectAllOption: true,
        templates: {
            li: '<li><a class="dropdown-item"><label class="m-0 pl-2 pr-0"></label></a></li>',
            ul: ' <ul class="multiselect-container dropdown-menu p-1 m-0"></ul>',
            button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown" data-flip="false"><span class="multiselect-selected-text"></span> <b class="caret"></b></button>',
            filter: '<li class="multiselect-item filter"><div class="input-group m-0"><input class="form-control multiselect-search" type="text"></div></li>',
            filterClearBtn: '<span class="input-group-btn"><button class="btn btn-secondary multiselect-clear-filter" type="button"><i class="fas fa-minus-circle"></i></button></span>'
        },
        buttonContainer: '<div class="dropdown" />',
        buttonClass: 'btn btn-secondary',
        
        enableFiltering: true,
        buttonWidth: '100%',
        maxHeight: 400,
        nonSelectedText: 'Tool Tag',
        onChange: function(element, checked) {
            tool = [];
            var brands = $('#multiselect2 option:selected');
            $(brands).each(function(index, brand){
                tool.push([$(this).text().trim()]);
            });
            console.log(tool);
        }
    });    
    $('#multiselect3').multiselect({
        includeSelectAllOption: true,
        templates: {
            li: '<li><a class="dropdown-item"><label class="m-0 pl-2 pr-0"></label></a></li>',
            ul: ' <ul class="multiselect-container dropdown-menu p-1 m-0"></ul>',
            button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown" data-flip="false"><span class="multiselect-selected-text"></span> <b class="caret"></b></button>',
            filter: '<li class="multiselect-item filter"><div class="input-group m-0"><input class="form-control multiselect-search" type="text"></div></li>',
            filterClearBtn: '<span class="input-group-btn"><button class="btn btn-secondary multiselect-clear-filter" type="button"><i class="fas fa-minus-circle"></i></button></span>'
        },
        buttonContainer: '<div class="dropdown" />',
        buttonClass: 'btn btn-secondary',
        
        enableFiltering: true,
        buttonWidth: '100%',
        maxHeight: 400,
        nonSelectedText: 'Subject Tag',
        onChange: function(element, checked) {
            subject = [];
            var brands = $('#multiselect3 option:selected');
            $(brands).each(function(index, brand){
                subject.push([$(this).text().trim()]);
            });
            console.log(subject);
        }
    });   
});

Dropzone.autoDiscover = false;

// init dropzone on id (form or div)
$(document).ready(function() {
    var myDropzone = new Dropzone("#myDropzone", {
        url: base_url+"home/file_upload",
        paramName: "file",
        autoProcessQueue: false,
        uploadMultiple: true, // uplaod files in a single request
        parallelUploads: 100, // use it with uploadMultiple
        maxFilesize: 1, // MB
        maxFiles: 10,
        acceptedFiles: ".jpg, .jpeg, .png",
        addRemoveLinks: true,
        // Language Strings
        dictFileTooBig: "File is to big ({{filesize}}mb). Max allowed file size is {{maxFilesize}}mb",
        dictInvalidFileType: "Invalid File Type",
        dictCancelUpload: "Cancel",
        dictRemoveFile: "Remove",
        dictMaxFilesExceeded: "Only {{maxFiles}} files are allowed",
        dictDefaultMessage: "Drop files here to upload",
    });
});

Dropzone.options.myDropzone = {
    // The setting up of the dropzone
    init: function() {
        var myDropzone = this;

            $.getJSON(base_url+"home/getMockup",{mockup_id:document.getElementById("mockup_id").value}, function( data ) {
            if (data == null) {
                        return ;
            }
            var file_data = data.mockup;
            $.each(file_data, function(key,value){

            let mockFile = { name: value.name,status: "added",size:value.size,dataURL: value.relURL,accepted: true, };
            
            myDropzone.emit("addedfile", mockFile);
            myDropzone.createThumbnailFromUrl(mockFile,
                myDropzone.options.thumbnailWidth, 
                myDropzone.options.thumbnailHeight,
                myDropzone.options.thumbnailMethod, true, function (thumbnail) {
                        myDropzone.emit('thumbnail', mockFile, thumbnail);
                });

             myDropzone.emit("success", mockFile);
             myDropzone.emit('complete', mockFile);
             myDropzone.files.push(mockFile);
            });

            });
           document.getElementById('form').addEventListener('submit',function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (myDropzone.getQueuedFiles().length > 0){                        
                        myDropzone.processQueue();  
            } else {                 
                submitMyFormWithData();
            }     

          });  
        // on add file
        this.on("addedfile", function(file) {
            console.log(file);
        });
        // on error
        this.on("error", function(file, response) {
            // console.log(response);
        });
        // on start
        this.on("sendingmultiple", function(file, xhr, formData) {
             // console.log(file);
            formData.append("mockup_id",document.getElementById("mockup_id").value.trim()); 
            formData.append("project_title",document.getElementById("project_title").value); 
            formData.append("svn_path",document.getElementById("svn_path").value);  
            formData.append("review_link",document.getElementById("review_link").value);  
            formData.append("description",document.getElementById("description").value);  
            formData.append("tags","");
            formData.append("level",document.getElementById("level").value);
            formData.append("dvlpmt",document.getElementById("dvlpmt").value);
            formData.append("solution",solution);
            formData.append("tool",tool);
            formData.append("subject",subject);
            formData.append("review_link",document.getElementById("review_link").value); 

        });
        // on success
        this.on("successmultiple", function(file) {
            $('#form').prepend('<div class="alert alert-success alert-dismissible fade show" id="alert" role="alert"> <strong>Success!</strong> Upload files sccucessfully updated.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            $("html, body").animate({ scrollTop: 0 }, "fast");
            window.setTimeout(function() {
                window.location.href = base_url+'home/dashboard';
            }, 3500);
        });
        this.on("removedfile",function(file) {
            var name = file.name; 
            var id = document.getElementById("mockup_id").value;
            $.ajax({
             type: 'POST',
             url: base_url+'home/remove_file',
             data: {name: name,id:id},
             sucess: function(data){
                console.log('success: ' + data);
             }
            });
        });
    }
};

function submitMyFormWithData(){
        formData = new FormData();
        formData.append("mockup_id",document.getElementById("mockup_id").value.trim()); 
        formData.append("project_title",document.getElementById("project_title").value); 
        formData.append("svn_path",document.getElementById("svn_path").value);  
        formData.append("description",document.getElementById("description").value);  
        formData.append("tags","");
        formData.append("level",document.getElementById("level").value);
        formData.append("dvlpmt",document.getElementById("dvlpmt").value);
        formData.append("solution",solution);
        formData.append("tool",tool);
        formData.append("subject",subject);
        formData.append("review_link",document.getElementById("review_link").value); 

        $.ajax({
                url: base_url+'home/update_mockup_data',
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data){
                    $('#form').prepend('<div class="alert alert-success alert-dismissible fade show" id="alert" role="alert"> <strong>Success!</strong> Data sccucessfully updated.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                    $("html, body").animate({ scrollTop: 0 }, "fast");
                    window.setTimeout(function() {
                        window.location.href = base_url+'home/dashboard';
                    }, 2000);
                }
        });
    }