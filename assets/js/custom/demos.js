var base_url = $("#base_url").val();

$('.demo-msg').unbind('click').bind('click', function (e) {
    e.preventDefault();
    var index =$(this).data("id");
    if (confirm('Are you sure you want to delete this?')) {
        $.ajax({
            url: $(this).attr("href"),
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {
                $('#demo_'+index).slideUp();
            }
        });
    }
});

function edit_demo(demo_id){
    $.ajax({
        'type': 'POST',
        'url': base_url+'admin_home/getDemoDetails',
        'data': {'demo_id': demo_id},
        'dataType': 'text',
        'cache': false,
        'success': function(data){
            $('#edit-demo .modal-body').html(data);
            $('#edit-demo').modal('show');
        }
    });
}