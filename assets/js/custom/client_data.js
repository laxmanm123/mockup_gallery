var base_url = $("#base_url").val();

function edit_image(image_id){
    $.ajax({
        'type': 'POST',
        'url': base_url+'admin_home/getClientImageDetails',
        'data': {'image_id': image_id},
        'dataType': 'text',
        'cache': false,
        'success': function(data){
            $('#edit-image .modal-body').html(data);
            $('#edit-image').modal('show');
        }
    });
}

$('.image-del').unbind('click').bind('click', function (e) {
    e.preventDefault();
    var index =$(this).data("id");
    if (confirm('Are you sure you want to delete this?')) {
        $.ajax({
            url: $(this).attr("href"),
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {
                $('#image_'+index).slideUp();
            }
        });
    }
});