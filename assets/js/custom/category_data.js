var base_url = $("#base_url").val();
$("#type").hide();

$("#type_id").change(function(){
	var type_id = $("#type_id").val();
	if (type_id == 'A') {
		$("#type").show();
	}else {
		$("#type").hide();
	}
});

$("#submit").hide();
$("#icon").change(function(){
    var img = document.getElementById('icon');

    readIMG(this);
    
    var _URL = window.URL || window.webkitURL;
    var image, logo;
    if ((logo = this.files[0])) {
        image = new Image();
        image.onload = function() {
            var width = this.width;
            var height = this.height;

            if(width == 62 && height == 62){
                $("#res").html("");
                $("#submit").show();
                $("#fake").hide();
            }else {
                $("#res").html("Uploaded image size is incorrect..!!");
                $("#submit").hide();
                $("#fake").show();
            }
        };
        image.src = _URL.createObjectURL(logo);
    }
});

$("#icon").change(function(){
    var file = $("#icon").val();
    var exe = this.value.match(/\.(.+)$/)[1];

    if(file != '' && exe == 'png'){
        $("#submit").show();
        $("#fake").hide();
    }else {
        $("#res").html("This File type is not allowed. Only PNG allowed");
        $("#submit").hide();
        $("#fake").show();
    }
});

function readIMG(input){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
             $('#img_upload').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }else{
         $('#img_upload').attr('src', base_url+'assets/images/no-img.png');
         $("#res").html("");
    }
}

$('.video-del').unbind('click').bind('click', function (e) {
    e.preventDefault();
    var index =$(this).data("id");
    if (confirm('Are you sure you want to delete this?')) {
        $.ajax({
            url: $(this).attr("href"),
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {
                $('#video_'+index).slideUp();
            }
        });
    }
});

$('.image-del').unbind('click').bind('click', function (e) {
    e.preventDefault();
    var index =$(this).data("id");
    if (confirm('Are you sure you want to delete this?')) {
        $.ajax({
            url: $(this).attr("href"),
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {
                $('#image_'+index).slideUp();
            }
        });
    }
});

$('.cord-del').unbind('click').bind('click', function (e) {
    e.preventDefault();
    var index =$(this).data("id");
    if (confirm('Are you sure you want to delete this?')) {
        $.ajax({
            url: $(this).attr("href"),
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {
                $('#cord_'+index).slideUp();
            }
        });
    }
});

$('.tab-del').unbind('click').bind('click', function (e) {
    e.preventDefault();
    var index =$(this).data("id");
    if (confirm('Are you sure you want to delete this?')) {
        $.ajax({
            url: $(this).attr("href"),
            type: "POST",
            data: {},
            dataType: 'json',
            success: function (data) {
                $('#tab_'+index).slideUp();
            }
        });
    }
});

function edit_video(video_id){
    $.ajax({
        'type': 'POST',
        'url': base_url+'admin_home/getVideoDetails',
        'data': {'video_id': video_id},
        'dataType': 'text',
        'cache': false,
        'success': function(data){
            $('#edit-video .modal-body').html(data);
            $("#type1").hide();
            $('#edit-video').modal('show');
        }
    });
}

function edit_image(image_id){
    $.ajax({
        'type': 'POST',
        'url': base_url+'admin_home/getImageDetails',
        'data': {'image_id': image_id},
        'dataType': 'text',
        'cache': false,
        'success': function(data){
            $('#edit-image .modal-body').html(data);
            $('#edit-image').modal('show');
        }
    });
}

function edit_cord(cord_id){
    $.ajax({
        'type': 'POST',
        'url': base_url+'admin_home/getCordDetails',
        'data': {'cord_id': cord_id},
        'dataType': 'text',
        'cache': false,
        'success': function(data){
            $('#edit-cord .modal-body').html(data);
            $('#edit-cord').modal('show');
        }
    });
}

function edit_tab(tab_id){
    $.ajax({
        'type': 'POST',
        'url': base_url+'admin_home/getTabDetails',
        'data': {'tab_id': tab_id},
        'dataType': 'text',
        'cache': false,
        'success': function(data){
            $('#edit-tab .modal-body').html(data);
            $("#type1").hide();
            $('#edit-tab').modal('show');
        }
    });
}