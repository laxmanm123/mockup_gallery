<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=get_asset('images','favicon.png');?>">
    <title>Mockup Gallery</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=get_asset('plugins/bootstrap/css','bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=get_asset('css','style.min.css');?>" rel="stylesheet">
    <link href="<?=get_asset('css','animate.css');?>" rel="stylesheet">
    <link href="<?=get_asset('css','spinners.css');?>" rel="stylesheet">
    <link href="<?=get_asset('scss/icons/themify-icons','themify-icons.css');?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="<?=get_asset('scss/icons/font-awesome/css','font-awesome.min.css');?>" rel="stylesheet">
    <link href="<?=get_asset('scss/icons/simple-line-icons/css','simple-line-icons.css');?>" rel="stylesheet">
    <link href="<?=get_asset('scss/icons/weather-icons/css','weather-icons.min.css');?>" rel="stylesheet">
    <link href="<?=get_asset('scss/icons/linea-icons/','linea.css');?>" rel="stylesheet">
    <link href="<?=get_asset('scss/icons/flag-icon-css','flag-icon.min.css');?>" rel="stylesheet">
    <link href="<?=get_asset('scss/icons/material-design-iconic-font/css','materialdesignicons.min.css');?>" rel="stylesheet">

    <link href="<?=get_asset('css','custom.css');?>" rel="stylesheet">
    <link href="<?=get_asset('css', 'main.min.css');?>" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?=get_asset('css/colors','default.min.css');?>" id="theme" rel="stylesheet">
    
    <link href="<?=get_asset('plugins/dropzone/min','dropzone.min.css');?>" rel="stylesheet">
    <link href="<?=get_asset('plugins/bootstrap-multiselect/css','bootstrap-multiselect.css');?>" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar custom-topbar">
            <!--nav class="navbar top-navbar navbar-expand-md navbar-light"-->
            <nav class="navbar navbar-light navbar-expand-md bg-faded">   
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="">
                    <?=anchor('home','<img src="'.get_asset('images','logo.png').'" alt="Logo" class="nav-logo">','class="navbar-brand mr-auto"');?>
                </div>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
                 <span class="navbar-toggler-icon"></span>
               </button>
                <div class="navbar-collapse collapse" id="collapsingNavbar">
                    <? if($pg=="dashboard"):?>
                    <ul class="navbar-nav  ml-auto">
                        <li class="nav-item" style="width:500px;">
                            <form action="<?=base_url("home/dashboard");?>">
                                <div class="input-group h-100 w-100">
                                <input type="text" name="search" class="form-control custom-search h-100" placeholder="Search title and tags" value="<?=($search ? $search : ''); ?>" style="width:500px;">
                                <div class="input-group-append">
                                  <button class="btn btn-secondary" type="submit" style="height: 39px;">
                                    <i class="fa fa-search"></i>
                                  </button>
                                </div>
                              </div>
                            </form>
                        </li>
                    </ul>
                    <? endif; ?>
                    <? if($this->session->userdata('logged_in')): ?>
                    <ul class="navbar-nav ml-auto">
                        <? if($pg=="dashboard"):?>
                        <li class="nav-item mt-1">
                                <div class="d-inline-block p-2 dropdown">
                                <button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 200px; height: 39px;"><?=($user_id != $this->session->userdata('logged_in')['id']) ? $username : "View Others"; ?> <i class="fas fa-caret-down"></i></button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                      <? 
                                        if($strategy_team): 
                                            foreach ($strategy_team as $key => $user):
                                            echo '<a class="dropdown-item" href="'.base_url('home/dashboard/'.$user->id).'" data-id="$user->id">'.$user->name.'</a>';
                                            endforeach;
                                        endif;
                                      ?>
                                    </div>
                                </div>
                        </li>
                        <? if($is_active_upload): ?>
                        <li class="nav-item mt-1">
                            <div class="d-inline-block p-2">
                                <button type="button" class="btn btn-primary btn-sm" style="width: 200px; height: 39px;" onclick="window.location.href ='<?=base_url('home/upload'); ?>'">Upload Asset</button>
                            </div>
                        </li>
                        <? endif; endif; ?>
                        <li class="nav-item dropdown mt-1">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark p-2" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?=asset_url('images/avatar.jpg');?>" alt="user" class="profile-pic" />
                            </a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-text">
                                                <h4><?=$this->session->userdata('logged_in')['username'];?></h4>
                                                <p class="text-muted"><?=$this->session->userdata('logged_in')['email'];?></p>
                                            </div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><?=anchor('home/logout','<i class="ti-power-off"></i> Logout');?></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <? endif; ?>
                </div>
            </nav>
        </header>   
        <div class="page-wrapper" style="background: #2f3d4a;">