<div class="modal-header">
    <h4 class="modal-title"><?=$mockup->project_title; ?>- Discussion</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body" >
    <?= form_open(base_url('home/reply'), array('id' => 'reply-msg-form', 'role' => 'form')); ?>
    <input type="hidden" name="mockup_id" value="<?=$mockup->id?>" />
    <input type="hidden" id="modal-url" value="<?= base_url('home/reply_view/'.$mockup->id);?>" />
    <div class="row">
      <div class="card-body border-bottom">
          <div class="row">
              <div class="col-11">
                  <textarea placeholder="Type your commet here" class="form-control b-0 bg-light" id="reply-msg" name="msg" rows="4" required></textarea>
              </div>
              <div class="col-1 text-right">
                  <button type="submit" class="btn btn-info btn-circle btn-lg" id="reply-msg-save"><i class="fas fa-paper-plane"></i></button>
              </div>
              <div class="col-sm-12" id="reply-msg-error"></div>
          </div>
      </div>
    </div>
  </form>
  <div class="row">
    <? if($replies) { ?>
        <div class="chat-rbox">
          <ul class="chat-list p-20">
          <? foreach ($replies as $reply) { ?>
            <!--chat Row -->
            <? if ($reply->user_id == $user_id) { ?>
              <li class="reverse chat-reply" class="" id="reply_<?=$reply->id?>">
                  <div class="chat-content">
                      <h5><?=$reply->name;?></h5>
                      <div class="box bg-light-inverse text-left"><?=$reply->message; ?></div>
                  </div>
                  <div class="chat-img"><img src="<?= get_asset('images', 'chat-user.jpg') ?>" alt="user" /></div>
                  <div class="chat-time"><?=dashboard_date_format($reply->created); ?>&nbsp;<a href="<?= base_url('home/reply_delete/' . $reply->id.'/'.$mockup->id);?>" title="Delete" class="reply-msg text-danger"  data-id="<?=$reply->id?>"><i class="far fa-trash-alt"></i></a></div>
              </li>
            <? } else {  ?>
              <li class="chat-reply" id="reply_<?=$reply->id?>">
                    <div class="chat-img"><img src="<?= get_asset('images', 'chat-user.jpg') ?>" alt="user" /></div>
                    <div class="chat-content">
                        <h5><?=$reply->name;?></h5>
                        <div class="box bg-light-info"><?=$reply->message; ?></div>
                    </div>
                    <div class="chat-time"><?=dashboard_date_format($reply->created); ?></div>
              </li>
            <? } } ?>
            <!--end chat Row -->
          </ul>
      </div>
    <?} ?>
  </div>
</div>