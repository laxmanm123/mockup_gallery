<? $this->load->view('header'); ?>
<!-- This snippet uses Font Awesome 5 Free as a dependency. You can download it at fontawesome.io! -->
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Sign In</h5>
            <form action="<?=base_url('welcome/login_process'); ?>"class="form-signin"method="post">
              <div class="form-label-group">
              	<label for="inputEmail">Email address</label>
                <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
              </div>
              <div class="form-label-group">
              	<label for="inputPassword">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
              </div>

              <!--div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input" id="customCheck1">
                <label class="custom-control-label" for="customCheck1">Remember password</label>
              </div-->
              <div class="form-group mt-2">
                <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>  
              </div>           
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<? $this->load->view('footer'); ?>