            <footer class="footer text-left custom-footer">
                &copy; 2018 <a href="//www.eidesign.net" target="_blank">EI Design</a>. All Rights Reserved.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?=get_asset('plugins/jquery','jquery.min.js');?>"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=get_asset('plugins/bootstrap/js','popper.min.js'); ?>"></script>
    <script src="<?=get_asset('plugins/bootstrap/js','bootstrap.min.js');?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=get_asset('js','sidebarmenu.min.js');?>"></script>
    <script src="<?=get_asset('js','jquery.slimscroll.js');?>"></script>
    <!--Wave Effects -->
    <script src="<?=get_asset('js','waves.js');?>"></script>
    
    <!--stickey kit -->
    <script src="<?=get_asset('plugins/sticky-kit-master/dist','sticky-kit.min.js');?>"></script>
    <script src="<?=get_asset('plugins/sparkline','jquery.sparkline.min.js');?>"></script>
    <!--Custom JavaScript -->
    <script src="<?=get_asset('js','custom.min.js');?>"></script>
    <!--clipboard-->
    <script src="<?=get_asset('plugins/clipboard/dist','clipboard.min.js');?>"></script>
    <!--dropzone-->
    <script src="<?=get_asset('plugins/dropzone/min','dropzone.min.js');?>"></script>
    <!--bootstrap-multiselect-->
    <script src="<?=get_asset('plugins/bootstrap-multiselect/js','bootstrap-multiselect.min.js');?>"></script>
    <?if($pg=="dashboard"): ?>
    <script src="<?=get_asset('js/custom','main.js');?>"></script>
    <? endif; ?>
    <?if($pg=="upload"): ?>
    <script src="<?=get_asset('js/custom','dropzone-upload.js');?>"></script>
    <? endif; ?>
    <?if($pg=="editupload"): ?>
    <script src="<?=get_asset('js/custom','dropzone-edit-upload.js');?>"></script>
    <? endif; ?>
    <script type="text/javascript">
        var base_url = '<?=base_url();?>';
    </script>
</body>
</html>