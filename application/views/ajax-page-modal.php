<div class="modal-header">
    <div class="col-9">
      <h4 class="modal-title" id="myLargeModalLabel"><?=$mockup->project_title; ?> </h4>
    </div>
    <div class="col-3 text-right">
    <? if(($mockup->user_id == $this->session->userdata('logged_in')['id']) || ($this->session->userdata('logged_in')['email'] == "shilpas@eidesign.net")): ?>
      <button type="button" class="btn btn-primary btn-sm" onclick="window.location.href='<?=base_url('home/editupload/'.$mockup->id); ?>'">Edit</button>
      <button type="button" class="btn btn-primary btn-sm" onclick="redirect('<?=base_url('home/delete/'.$mockup->id); ?>')">Delete</button>
    <? endif; ?>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
  </div>
</div>
<div class="modal-body" style="max-height:550px;overflow-y: auto;">
  <div class="row">
    <div class="col-md-8">
      <div id="carouselExampleIndicators100" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <? $imgs = json_decode($mockup->mockup); if($imgs): foreach ($imgs as $key => $img) : ?>
          <li data-target="#carouselExampleIndicators100" data-slide-to="0" class="<?=(($key ==0) ? "active": ''); ?>"></li>
          <? endforeach; endif; ?>
        </ol>
        <div class="carousel-inner" role="listbox">
          <? $imgs = json_decode($mockup->mockup); if($imgs): foreach ($imgs as $key => $img) : ?>
          <div class="carousel-item <?=(($key ==0) ? "active": ''); ?>">
            <img class="img-responsive" src="<?=asset_url('uploads/'.$img);?>" alt="First slide">
          </div>
          <? endforeach; endif; ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators100" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators100" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <label class="font-weight-bold">Description:</label>
      <p><?=$mockup->description; ?></p>
      <label class="font-weight-bold">SVN Path:</label>&nbsp;<i class="far fa-copy" id="clipboard-btn"   data-clipboard-target="#copy-bar" style="cursor: pointer;" title="copy"></i>
      <p id="copy-bar" style="word-break: break-all;font-size:12px;"><?=$mockup->svn_path; ?></p>
      <label class="font-weight-bold">Review Link:</label>&nbsp;<i class="far fa-copy" id="clipboard-btn2"   data-clipboard-target="#copy-bar" style="cursor: pointer;" title="copy"></i>
      <p id="copy-bar2" style="word-break: break-all;font-size:12px;"><?=$mockup->review_link; ?></p>

      <label class="font-weight-bold">Solution:</label>
      <p>
        <?php $arr = explode(',', $mockup->solution);
          foreach ($arr as $key => $value) {
            # code...
            if ($value == $searchKeyword) {
              # code...
              echo '<label class="label label-warning">'.$value.'</label> ';
            }else {
              echo '<label class="label label-info">'.$value.'</label> ';
            }
          }
        ?>
      </p>
      <label class="font-weight-bold">Tool:</label>
      <p>
        <?php $arr = explode(',', $mockup->tool);
          foreach ($arr as $key => $value) {
            # code...
            if ($value == $searchKeyword) {
              # code...
              echo '<label class="label label-warning">'.$value.'</label> ';
            }else {
              echo '<label class="label label-info">'.$value.'</label> ';
            }
          }
        ?>
      </p>
      <label class="font-weight-bold">Subject:</label>
      <p>
        <?php $arr = explode(',', $mockup->subject);
          foreach ($arr as $key => $value) {
            # code...
            if ($value == $searchKeyword) {
              # code...
              echo '<label class="label label-warning">'.$value.'</label> ';
            }else {
              echo '<label class="label label-info">'.$value.'</label> ';
            }
          }
        ?>
      </p>
      <label class="font-weight-bold">Updated By:</label>
      <p><?=$mockup->name." (".$mockup->created_on.")"; ?></p>
      <? if($is_comment_enable){ ?> 
      <a href="<?=base_url('home/reply_view/'.$mockup->id); ?>" class="btn btn-inverse btn-sm" data-toggle="modal" data-target="#reply-msg-popup">Comments</a>   
      <? } ?>
    </div>
    </div>
  </div>
</div>