<? $this->load->view('header'); ?>

<section class="container-fluid p-2">
  <div class="row el-element-overlay mt-2">
    <div class="col-12">
      <h4>Upload Mockup</h4>
    </div>
    <div class="col-12">
     <form action="<?=base_url('home/upload_process'); ?>" method="post" id="form" enctype="multipart/form-data" autocomplete="off">
      <div class="form-row">
        <div class="form-group col-md-4">
                <input type="text" name="project_title" id="project_title" class="form-control" placeholder="Project Title" required="" />
        </div>
        <div class="form-group col-md-4">
            <select name="level" class="form-control" id="level" required="">
              <option>Select Level</option>
              <? foreach ($level_tags as $key => $value) {
                echo "<option value=".$value.">".$value."</option>";
              } ?>
            </select>
        </div>
        <div class="form-group col-md-4">
            <select name="dvlpmt" id="dvlpmt" class="form-control" required="">
              <option>Select Developement Type</option>
              <? foreach ($developement_type_tags as $key => $value) {
                echo "<option value=".$value.">".$value."</option>";
              } ?>
            </select>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
            <select name="solution" id="multiselect1" class="multiselect-ui form-control" multiple="multiple" required="">
              <? foreach ($solution_tags as $key => $value) {
                echo "<option value=".$value.">".$value."</option>";
              } ?>
            </select>
        </div>
        <div class="form-group col-md-4">
            <select name="tool" id="multiselect2" class="multiselect-ui form-control" multiple="multiple" required="">
              <? foreach ($tool_tags as $key => $value) {
                echo "<option value=".$value.">".$value."</option>";
              } ?>
            </select>
        </div>
        <div class="form-group col-md-4">
            <select name="subject" id="multiselect3" class="multiselect-ui form-control" multiple="multiple" required="">
              <? foreach ($subject_tags as $key => $value) {
                echo "<option value=".$value.">".$value."</option>";
              } ?>
            </select>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <input type="text" name="svn_path" id="svn_path" class="form-control" placeholder="SVN Path" required="" />
        </div>
        <div class="form-group col-md-6">
          <input type="text" name="review_link" id="review_link" class="form-control" placeholder="Review Link"/>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-12">
          <textarea name="description" name="description" id="description" class="form-control" placeholder="Description" required=""></textarea>
        </div>
        <div class="col-12">
          <div  class="dropzone form-group" id="myDropzone" name="mainFileUploader">
              <div class="fallback">
                  <input name="files" type="file" multiple required="" />
              </div>
              <div class="dz-message d-flex flex-column text-center">
                <i class="fas fa-cloud-upload-alt"></i>
                Drag &amp; Drop here or click <span class="text-danger">Max allowed file size is 1MB</span>
              </div>
          </div>
        </div>
      </div>
        <button type="Submit" id="dropzoneSubmit" class="btn btn-success">Submit</button>
        <button type="button" class="btn btn-light" onclick="javascript:history.go(-1)">Cancel</button>
  </form>
  </div>
  </div>
</section>
<? $this->load->view('footer'); ?>