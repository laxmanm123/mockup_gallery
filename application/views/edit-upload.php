<? $this->load->view('header'); ?>

<section class="container-fluid p-2">
  <div class="row el-element-overlay mt-2">
    <div class="col-12">
      <h4>Edit Upload Mockup</h4>
    </div>
    <div class="col-12">
    <form action="<?=base_url('home/upload_process'); ?>" method="post" id="form" enctype="multipart/form-data" autocomplete="off">
      <input type="hidden" name="id" id="mockup_id"value="<?=$mockup_data->id; ?>">
      <div class="form-row">
        <div class="form-group col-md-4">
                <input type="text" name="project_title" id="project_title" class="form-control" placeholder="Project Title" required="" value="<?=($mockup_data->project_title ? $mockup_data->project_title:null );?>" />
        </div>
        <div class="form-group col-md-4">
            <select name="level" class="form-control" id="level" required="">
              <option>Select Level</option>
              <? foreach ($level_tags as $key => $value) {
                echo "<option value='".$key."' ".($mockup_data->level == $key ? "selected" : null).">".$value."</option>";
              } ?>
            </select>
        </div>
        <div class="form-group col-md-4">
            <select name="dvlpmt" id="dvlpmt" class="form-control" required="">
              <option>Select Developement Type</option>
              <? foreach ($developement_type_tags as $key => $value) {
                echo "<option value=".$key." ".($mockup_data->level == $key ? "selected" : null).">".$value."</option>";
              } ?>
            </select>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
            <select name="solution" id="multiselect1" class="multiselect-ui form-control" multiple="multiple" required="">
              <? foreach ($solution_tags as $key => $value) {
                echo "<option ".((in_array(trim($value), $selected_solution_tags)) ? "selected": "")." value=".$value.">".$value."</option>";
              } ?>
            </select>
        </div>
        <div class="form-group col-md-4">
            <select name="tool" id="multiselect2" class="multiselect-ui form-control" multiple="multiple" required="">
              <? foreach ($tool_tags as $key => $value) {
                echo "<option ".((in_array(trim($value), $selected_tool_tags)) ? "selected": "")." value=".$value.">".$value."</option>";
              } ?>
            </select>
        </div>
        <div class="form-group col-md-4">
            <select name="subject" id="multiselect3" class="multiselect-ui form-control" multiple="multiple" required="">
              <? foreach ($subject_tags as $key => $value) {
                echo "<option ".((in_array(trim($value), $selected_subject_tags)) ? "selected": "")." value=".$value.">".$value."</option>";
              } ?>
            </select>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <input type="text" name="svn_path" id="svn_path" class="form-control" placeholder="SVN Path" required="" value="<?=($mockup_data->svn_path ? $mockup_data->svn_path:null );?>"/>
        </div>
        <div class="form-group col-md-6">
          <input type="text" name="review_link" id="review_link" class="form-control" placeholder="Review Link" value="<?=($mockup_data->review_link ? $mockup_data->review_link:null );?>"/>
        </div>
      </div>
      <div class="form-group">
        <textarea name="description" name="description" id="description" class="form-control" placeholder="Description" required=""><?=($mockup_data->description ? $mockup_data->description:null );?></textarea>
      </div>
      <div  class="dropzone form-group" id="myDropzone" name="mainFileUploader">
              <div class="fallback">
                  <input name="files" type="file" multiple required="" />
              </div>
              <div class="dz-message d-flex flex-column text-center">
                <i class="fas fa-cloud-upload-alt"></i>
                Drag &amp; Drop here or click
              </div>
      </div>
      <button type="Submit" id="dropzoneSubmit" class="btn btn-success">Submit</button>
      <button type="button" class="btn btn-light" onclick="javascript:history.go(-1)">Cancel</button>

    </form>
  </div>
</div>
</section>
<? $this->load->view('footer'); ?>