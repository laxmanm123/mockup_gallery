   <? if($mockups_data):
  foreach ($mockups_data as $key => $data): ?>
      <div class="col-lg-3 col-md-6">
        <div class="card custom-img-card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> 
                    <? $imgs = ($data->mockup) ? json_decode($data->mockup) : null; ?> 
                    <img src="<?=(($imgs) ? asset_url('uploads/thumbnail/'.$imgs[0]) : asset_url('images/no-img.png'));?>" class="img-responsive">
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li>
                                <a class="btn btn-icon btn-pure btn-outline show-modal" data-id="<?=$data->id;?>" data-search-keyword="<?=($search ? $search : ''); ?>"><i class="ti-image" aria-hidden="true"></i> </a>
                                <? if($is_comment_enable){ ?> 
                                <a href="<?=base_url('home/reply_view/'.$data->id); ?>" class="btn btn-icon btn-pure btn-outline" data-toggle="modal" data-target="#reply-msg-popup"><i class="fa fa-comments"></i></a>
                                <? } ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h6 class="box-title"><?=(!empty($data->project_title)? highlightKeywords($data->project_title,($search ? $search : '')): null)?></h6>
                </div>
            </div>
          </div>
        </div>
<? endforeach; endif; ?>
<div class="col-md-12">
  <?php echo $this->ajax_pagination->create_links(); ?>
</div>