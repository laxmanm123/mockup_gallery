<? $this->load->view('header'); ?>

<section class="container-fluid p-0">

<div class="row">
  <div class="col-md-3 bg-white p-0">
  <!-- Section: Filters -->
  <section class="filter"> 
    <div class="filterheading">
      <div class="filterheadingText">
        <h5 class="text-dark font-weight-bold p-3">Filters</h5> 
      </div>
    </div>

    <!-- Section: Size -->
    <section class="mb-4 p-3">

     <div id="vertical-menu">
        <ul>
            <li>
                  <h3>Dashboard<span class="plus">+</span></h3>

                <ul>
                    <li><a href="#">Reports</a>
                    </li>
                    <li><a href="#">Search</a>
                    </li>
                    <li><a href="#">Graphs</a>
                    </li>
                    <li><a href="#">Settings</a>
                    </li>
                </ul>
            </li>
            <!-- we will keep this LI open by default -->
            <li>
                  <h3>Tasks<span class="plus">+</span></h3>

                <ul>
                    <li><a href="#"><input type="checkbox" class="">Today's tasks</a>
                    </li>
                    <li><a href="#">Urgent</a>
                    </li>
                    <li><a href="#">Overdues</a>
                    </li>
                    <li><a href="#">Recurring</a>
                    </li>
                    <li><a href="#">Settings</a>
                    </li>
                </ul>
            </li>
            <li>
                  <h3>Favorites<span class="plus">+</span></h3>

                <ul>
                    <li><a href="#">Global favs</a>
                    </li>
                    <li><a href="#">My favs</a>
                    </li>
                    <li><a href="#">Team favs</a>
                    </li>
                    <li><a href="#">Settings</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>

    </section>
    <!-- Section: Size -->

  </section>
  <!-- Section: Filters -->

  </div>
  <div class="col-md-9">
      <div class="row el-element-overlay mt-2" id="mockupList">
  <? if($mockups_data):
  foreach ($mockups_data as $key => $data): ?>
      <div class="col-lg-3 col-md-6">
        <div class="card custom-img-card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> 
                    <? $imgs = ($data->mockup) ? json_decode($data->mockup,true) : null; ?> 
                    <img src="<?=(($imgs) ? asset_url('uploads/thumbnail/'.$imgs[0]) : asset_url('images/no-img.png'));?>" class="img-responsive" max>
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li>
                                <a class="btn btn-icon btn-pure btn-outline show-modal" data-id="<?=$data->id;?>" data-search-keyword="<?=($search ? $search : ''); ?>"><i class="ti-image" aria-hidden="true"></i> </a>
                                <? if($is_comment_enable){ ?> 
                                <a href="<?=base_url('home/reply_view/'.$data->id); ?>" class="btn btn-icon btn-pure btn-outline" data-toggle="modal" data-target="#reply-msg-popup"><i class="fa fa-comments"></i></a>
                                <? } ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h6 class="box-title"><?=ucwords((!empty($data->project_title)? highlightKeywords($data->project_title,($search ? $search : '')): null));?></h6>
                </div>
            </div>
          </div>
        </div>
<? endforeach;  else:?>
      <div class="col-md-12 no-data border text-center bg-light" style="margin-top:10%;">
        <h1 class="mt-5 text-center"><i class="fas fa-database"></i></h1>
        <h2 class="mt-2 text-center">No Data</h2>
      </div>
<? endif;?>
<div class="col-md-12">
  <?php echo $this->ajax_pagination->create_links(); ?>
</div>
</div>
  </div>
</div>


</section>
<div class="modal fade" id="pageModal" tabindex="-1" role="dialog" aria-labelledby="pageModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
    </div>
  </div>
</div>
<div class="modal fade" id="reply-msg-popup" role="dialog" aria-labelledby="editProject" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="reply-modal-content"></div><!-- /.modal-content -->
    </div>
</div>

<? $this->load->view('footer'); ?>
