<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('mockup_model', '', TRUE);
		date_default_timezone_set('Asia/Kolkata');
	}

	private $key = 'DC302A95EB2309C5E001AFE8F286BA9A';
	private $cipher = "AES-128-CBC";

	private function decryptIt($encrypted_pwd){
		$c = base64_decode($encrypted_pwd);
		$ivlen = openssl_cipher_iv_length($this->cipher);
		$iv = substr($c, 0, $ivlen);
		$hmac = substr($c, $ivlen, $sha2len=32);
		$ciphertext_raw = substr($c, $ivlen+$sha2len);
		$decrypt = openssl_decrypt($ciphertext_raw, $this->cipher, $this->key, $options=OPENSSL_RAW_DATA, $iv);
		$calcmac = hash_hmac('sha256', $ciphertext_raw, $this->key, $as_binary=true);
		if (hash_equals($hmac, $calcmac)) {
			//PHP 5.6+ timing attack safe comparison
		    return $decrypt;
		}
	}

	private function hashPwd($pwd) {
		$options = array('cost'=>'12');
		return password_hash($pwd, PASSWORD_BCRYPT, $options);
	}
	
	function index() {
		$this->load->view('welcome_message');
	}

	function index_sso(){
		$encrypted = $_GET['token'];
		$user_det = json_decode($this->decryptIt($encrypted));
		
		// print_r($user_det);
		// exit;
		$res = $this->mockup_model->get_by_field('email', $user_det->email, 'users')->row();
		if ($res) {
			// User exists login
			if ($res->status) {
				# code...
				if (!$res->department_id) {
					# code...
					$det = array('department_id'=>$user_det->department_id);
					$this->mockup_model->update_det($det, $res->id, 'users');
				}
				
				$sess = array('id'=>$res->id, 'username'=>$res->name, 'email'=>$res->email, 'department_id'=>$res->department_id, 'user_type'=>$res->user_type,'is_loggedin' => true);
				$this->session->set_userdata('logged_in', $sess);

				if ($res->user_type === '0' || $res->user_type === '1') {
					# code...
					redirect('home');
				}else {
					redirect('home');
				}
			}else {
				redirect('home');
			}
		}else {
			// User dosen't exists, Register
			$det = array('name'=>$user_det->name, 'email'=>$user_det->email, 'department_id'=>$user_det->department_id, 'password'=>$this->hashPwd('Eidesign@123'), 'created_by'=>'1', 'created_on'=>date('Y-m-d H:i:s'));
			$uid = $this->mockup_model->insert_det($det, 'users');

			$sess = array('id'=>$uid, 'username'=>$user_det->name, 'email'=>$user_det->email, 'department_id'=>$res->department_id, 'user_type'=>'2','is_loggedin' => true);
			$this->session->set_userdata('logged_in', $sess);

			redirect('home');
		}
	}

	public function login_process(){

		    $email = $this->input->post('email');
		    $password = $this->input->post('password');

			$res = $this->mockup_model->get_by(array('email' => $email,),'users');
			if ($res) {
				if ($res->status){
					if (password_verify($password, $res->password)){
						$sess = array('id'=>$res->id, 'username'=>$res->name, 'name'=>$res->name, 
										'email'=>$res->email, 'department_id'=>$res->department_id,'user_type'=>$res->user_type,'is_loggedin' => true);
						
						$this->session->set_userdata('logged_in', $sess);
						
						if ($res->user_type === '0' || $res->user_type === '1') {
							# code...
							redirect('home');
						}else {
							redirect('home');							
						}
					}else {
						redirect('home');
					}
				}else {
					redirect('home');
				}
			}else {
				redirect('home');
			}
		
	}
}
