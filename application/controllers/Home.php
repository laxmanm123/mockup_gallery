<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('mockup_model');
        $this->load->library('Ajax_pagination');
        $this->perPage = 8;

        $this->tags = array("Microlearning Course","Microlearning Video","Gamification ","Personalisation","Mobile Apps","Virtual Reality","Gamification Portal","Scenario Based Learning","Application Simulation","Branching Simulation","Interactive Videos","Blended","ILT","Compliance ","Awareness Building ","Change Management","Soft Skills","Learning Portal","Responsive Mobile First ","Video Based Learning","Storytorial","Induction and Onboarding","Sales training","Brand Awareness","Leadership Training","Manager Training","Product Training","Process Training","Motivational and Aspirational","Professional Skills","CrossKnowledge Mohive","Articulate Storyline","Lectora ","Captivate","Articulate Rise","Migration");

        $this->solution_tags = array("Microlearning Course","Microlearning Video","Gamification","Personalisation ","Mobile App","Virtual Reality","Gamification Portal","Scenario Based Learning","Branching scenario","Application Simulation","Interactive Videos","Blended Learning","ILT","Learning Portal","Responsive Mobile First","Video Based Learning","Storytorial");

        $this->level_tags = array("Level 1", "Level 2","Level 3");


        $this->tool_tags = array("Storyline","Captivate","InDesign","Articulate Rise","Custom HTML","Parallax","Adapt","After Effect","Vyond","Cross Knowledge Mohive","Lectora","Dominknow Flow","Migration");

        $this->subject_tags = array("Compliance","Awareness Building","Change management","Soft Skills","Induction and ","Onboarding Sales Training","Brand Awareness","Leadership Training","Manager Training","Product Training","Process Training","Motivational and Aspirational","Professional Skills");

        $this->developement_type_tags = array("Custom","Semi Custom","Rapid Development");

        $this->data['is_comment_enable'] = (($this->session->userdata('logged_in')['department_id']==3)  || (in_array($this->session->userdata('logged_in')['email'], power_user_list()))  ? true : false); 
    }
	
	public function index(){

		$this->data['pg'] = "index";
		if($this->session->userdata('logged_in')){
			redirect('home/dashboard');
		}else
		$this->load->view('index',$this->data);
	}


	public function dashboard($user_id = null){

		$this->data['search'] = $this->input->get('search');
		$this->data['pg'] = "dashboard";
		if(!$this->session->userdata('logged_in')){
			redirect('home');
		}
		
		$strategy_team = $this->mockup_model->get_all(array('department_id'=>3,"status"=>"1"),'users')->result();
		#echo $this->db->last_query();die();
		$this->data['strategy_team'] = $strategy_team;

		if(!$user_id){
			$this->data['user_id'] = $this->session->userdata('logged_in')['id'];
			$this->data['username'] = $this->session->userdata('logged_in')['username'];
		}else{
			$key = array_search($user_id, array_column($this->data['strategy_team'], 'id'));
			$this->data['user_id']  = $user_id;
			$this->data['username'] = $this->data['strategy_team'][$key]->name;
		}
		
		$this->data['is_active_upload'] = (($this->session->userdata('logged_in')['department_id']==3)  || (in_array($this->session->userdata('logged_in')['email'], power_user_list()))  ? true : false);

		//$check_user_department = $this->mockup_model->get_by(array('department_id'=>3, 'id'=>$this->session->userdata('logged_in')['id']),'users');
		//$this->data['is_active_upload'] = $check_user_department ? true : false;


		$where = array();
		$where['status'] = 1;

		if($user_id || ($this->session->userdata('logged_in')['department_id']==3)){
			$where['user_id'] = $this->data['user_id'];
		}
		if($this->data['search']){
			$where['search'] = $this->data['search'];
		}
		
        $mockups = $this->mockup_model->getRows($where);
        //echo $this->db->last_query();die();
        //total rows count
        $totalRec = $mockups ? count($mockups) : 0;
        
        //Pagination configuration
        $config['target']      = '#mockupList';
        $config['base_url']    = base_url().'home/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $config['user_id'] = ($user_id || ($this->session->userdata('logged_in')['department_id']==3)) ? $this->data['user_id'] : '';
        $config['search'] = trim($this->data['search']);
        if($user_id){
        	$config['uri_segment']    = 4;
        }
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $where["limit"] = $this->perPage;
        $where['status'] = 1;
        $mockups_data = $this->mockup_model->getRows($where);
		//echo $this->db->last_query();die();

		$this->data['mockups_data'] = $mockups_data;

        //load the view
		$this->load->view('dashboard', $this->data);
	}

	public function ajaxPaginationData(){

		$this->data['user_id'] = $this->input->get('user_id');
		$this->data['search'] = $this->input->get('search');
        $page = $this->input->get('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        $where = array();
        $where['status'] = 1;
        if($this->data['user_id'] || ($this->session->userdata('logged_in')['department_id']==3)){
        	$where['user_id'] = $this->data['user_id'];
    	}
		if($this->data['search']){
			$where['search'] = trim($this->data['search']);
		}
        $mockups = $this->mockup_model->getRows($where);
        $totalRec = $mockups ? count($mockups) : 0;
        
        //pagination configuration
        $config['target']      =  '#mockupList';
        $config['base_url']    = base_url().'home/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $config['user_id']    = $this->data['user_id'];
        $config['search'] = trim($this->data['search']);
        $this->ajax_pagination->initialize($config);
        
        //get the mockups data
        $where["start"] = $offset;
        $where["limit"] = $this->perPage;
        $mockups_data  = $this->mockup_model->getRows($where);
        //echo $this->db->last_query();die();
        
		$this->data['mockups_data'] = $mockups_data;
		// print_r($this->data['mockups_data']);
		// exit;
        //load the view
        $this->load->view('ajax-pagination-data', $this->data, false);
    }
    public function ajaxPageModal(){
    	$id = $this->input->get('id');
    	$this->data['searchKeyword'] = $this->input->get('searchKeyword');
    	$this->data['mockup'] = $this->mockup_model->get_mockup_detail($id);
    	$dt = new DateTime($this->data['mockup']->created_on);
    	$this->data['mockup']->created_on =  $dt->format('F j, Y'); 
    	$this->data['is_comment_enable'] = (($this->session->userdata('logged_in')['department_id']==3)  || (in_array($this->session->userdata('logged_in')['email'], power_user_list()))  ? true : false);    	
    	$this->load->view('ajax-page-modal', $this->data, false);
    }

	public function upload(){

		$this->data['pg'] = "upload";
		if(!$this->session->userdata('logged_in')){
			redirect('home');
		}
		
		$user_id = $this->session->userdata('logged_in')['id'];

		$this->data['user_id'] = $this->session->userdata('logged_in')['id'];
			$this->data['username'] = $this->session->userdata('logged_in')['username'];
			

		$this->data['tags'] = $this->tags;
		
		$this->data['solution_tags'] = $this->solution_tags;
		$this->data['level_tags'] = $this->level_tags;
		$this->data['tool_tags'] = $this->tool_tags;
		$this->data['subject_tags'] = $this->subject_tags;
		$this->data['developement_type_tags'] = $this->developement_type_tags;
		$this->load->view('upload',$this->data);
	}



	public function editupload($id){

		$this->data['pg'] = "editupload";
		if(!$this->session->userdata('logged_in')){
			redirect('home');
		}
		
		$user_id = $this->session->userdata('logged_in')['id'];
		
		//print_r($this->data['projects']);exit;

		$mockup_data = $this->mockup_model->get_by(array('id'=>$id));

		//echo $this->db->last_query();die();
		$this->data['selected_tags'] = explode(",",$mockup_data->tags);
		$this->data['selected_solution_tags'] = explode(",",$mockup_data->solution);
		$this->data['selected_tool_tags'] = explode(",",$mockup_data->tool);
		$this->data['selected_subject_tags'] = explode(",",$mockup_data->subject);
		$this->data['mockup_data']= $mockup_data;

		// print_r($this->data['selected_tags']);
		// exit;
		$this->data['tags'] = $this->tags;

		$this->data['solution_tags'] = $this->solution_tags;
		$this->data['level_tags'] = $this->level_tags;
		$this->data['tool_tags'] = $this->tool_tags;
		$this->data['subject_tags'] = $this->subject_tags;
		$this->data['developement_type_tags'] = $this->developement_type_tags;

		$this->load->view('edit-upload',$this->data);


	}

	public function getMockup(){


		$mockup_id = $this->input->get('mockup_id');
		$user_id = $this->session->userdata('logged_in')['id'];
		$mockups_data = $this->mockup_model->get_by(array('id'=>$mockup_id));

		$rowst = array();
		$mockup = json_decode($mockups_data->mockup);
		
		if($mockup){
				foreach ($mockup as $key => $data) {
					$row = array();
					$row['name'] = $data;
					$file_path = 'assets/uploads/'.$data;
					// A few settings
					$row['path'] = asset_url('uploads/') ;
		    		$info      = getimagesize($file_path);
					$row['size'] = filesize($file_path);
					$row['type'] =  $info['mime']; // mime-type as string for ex. "image/jpeg" etc.
					$row['status'] = "added";

					// Read image path, convert to base64 encoding
					$imageData = base64_encode(file_get_contents(asset_url('uploads/'.$data)));

					// Format the image SRC:  data:{mime};base64,{data};
					$src = 'data: '.$info['mime'].';base64,'.$imageData;

					$row['relURL'] = $src;
						
					array_push($rowst, $row);

				}
				//$result_mockup = json_encode($rowst);
				echo json_encode(array('mockup' => $rowst,));
				exit;

		}
	
	}
	public function remove_file(){

	    $id = $this->input->post('id');
	    $name = $this->input->post('name');
	    $result = $this->mockup_model->get_by(array('id'=>$id));
	    if($result){
	    	$mockpups = json_decode($result->mockup,true);
	    	foreach ($mockpups as $key => $value) {
	    		if($value == $name){
	    			unset($mockpups[$key]);
	    		}
	    	}
	    	array_values($mockpups);
	    	$data['mockup'] = json_encode($mockpups,true);
			$this->mockup_model->update($id, $data);
	    }
	}
	public function upload_process(){
		print_r($this->input->post());
		exit;
	}
	public function file_upload(){

		// If file upload form submitted
        if(!empty($_FILES['file']['name'])){
            $filesCount = count($_FILES['file']['name']);
            for($i = 0; $i < $filesCount; $i++){
              	//exit;
                $_FILES['uploadFile']['name'] = str_replace(",","_",$_FILES['file']['name'][$i]);
				$_FILES['uploadFile']['type'] = $_FILES['file']['type'][$i];
				$_FILES['uploadFile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
				$_FILES['uploadFile']['error'] = $_FILES['file']['error'][$i];
				$_FILES['uploadFile']['size'] = $_FILES['file']['size'][$i];
	            //Directory where files will be uploaded
                $uploadPath = 'assets/uploads/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png';
                //$config['create_thumb'] = TRUE; 

                // Load and initialize upload library
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                // Upload file to server
                if($this->upload->do_upload('uploadFile')){
                    // Uploaded file data
                    $image_data = $this->upload->data();

                    $image_config["image_library"] = "gd2";
					$image_config["source_image"] = $image_data["full_path"];
					$image_config['create_thumb'] = FALSE;
					$image_config['maintain_ratio'] = TRUE;
					$image_config['new_image'] = $image_data["file_path"]."/thumbnail/" . $image_data["file_name"];
					$image_config['quality'] = "100%";
					$image_config['width'] = 200;
					$image_config['height'] = 200;
					$dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($image_config['width'] / $image_config['height']);
					$image_config['master_dim'] = ($dim > 0)? "height" : "width";
					 
					$this->load->library('image_lib');
					$this->image_lib->initialize($image_config);
					 
					if(!$this->image_lib->resize()){ //Resize image
					    $message ="Error";
					}else{
					}
					$this->image_lib->clear();
                    $uploadFiles[$i] = $image_data['file_name'];
                }
            }

            if(!empty($uploadFiles)){
                // Insert files data into the database

            	$uploadedData['user_id'] = $this->session->userdata('logged_in')['id'];
                $uploadedData['project_title'] = trim($this->input->post('project_title'));
                $uploadedData['svn_path'] = $this->input->post('svn_path');
                $uploadedData['description'] = $this->input->post('description');
				$uploadedData['mockup'] =  json_encode($uploadFiles,true);
				$uploadedData['tags'] = $this->input->post('tags');
				$uploadedData['level'] = $this->input->post('level');
				$uploadedData['dvlpmt'] = $this->input->post('dvlpmt');
				$uploadedData['solution'] = $this->input->post('solution');
				$uploadedData['tool'] = $this->input->post('tool');
				$uploadedData['subject'] = $this->input->post('subject');
				$uploadedData['review_link'] = $this->input->post('review_link');

                $this->load->model('mockup_model');
                if($this->input->post('mockup_id')){
                	$mockup_id = trim($this->input->post('mockup_id'));
					$result = $this->mockup_model->get_by(array('id'=>$mockup_id));
				    if(!empty($result->mockup) && $result->mockup !="null"){
				    	$uploadedData['mockup'] = json_encode(array_merge(json_decode($result->mockup, true),json_decode($uploadedData['mockup'], true)));
				    }
				    $uploadedData['updated_on']= current_date();		
				    $result = $this->mockup_model->update($mockup_id, $uploadedData);
				    //echo $this->db->last_query();die();
                }else{
                	 $result = $this->mockup_model->insert($uploadedData);
                }
                $statusMsg = $result?'Files uploaded successfully.':'Some problem occurred, please try again.';
            }
            else{
            	 $statusMsg = 'Files not uploaded';
            }
        }else{
        	$statusMsg = 'Not Uploaded File'; 
        }      
        
        echo json_encode(array("message"=>$statusMsg),true);
       	exit;
	}

	public function update_mockup_data(){
		$uploadedData['user_id'] = $this->session->userdata('logged_in')['id'];
        $uploadedData['project_title'] = trim($this->input->post('project_title'));
        $uploadedData['svn_path'] = $this->input->post('svn_path');
        $uploadedData['description'] = $this->input->post('description');
		$uploadedData['tags'] = $this->input->post('tags');
		$uploadedData['level'] = $this->input->post('level');
		$uploadedData['dvlpmt'] = $this->input->post('dvlpmt');
		$uploadedData['solution'] = $this->input->post('solution');
		$uploadedData['tool'] = $this->input->post('tool');
		$uploadedData['subject'] = $this->input->post('subject');
		$uploadedData['review_link'] = $this->input->post('review_link');
		$mockup_id =  $this->input->post('mockup_id');
		$result = $this->mockup_model->update($mockup_id ,$uploadedData);
		$statusMsg = $result?'uploaded successfully.':'Some problem occurred, please try again.';
		echo json_encode(array("message"=>$statusMsg),true);
       	exit;
	}

	// Comment & Reply 

	function reply(){


	$this->load->model('comment_model');	

    $response = array( 'status' => false);
    if ($this->input->post('reply_id')) {
      $result = '';
      $data = array('message' => $this->input->post('msg'), 'updated' => current_date());

      $this->comment_model->update($this->input->post('reply_id'), $data);
      if ($this->input->post('mockup_id') != $this->input->post('reply_id')) {
        $this->comment_model->update($this->input->post('mockup_id'), array('updated' => current_date()));
        $result = $this->comment_model->get_edited_row($this->input->post('mockup_id'));
      } else {
        $result = $this->comment_model->get_edited_row($this->input->post('reply_id'));
      }
      $base_url = base_url();
      $this->_table_data($result, $base_url);
      $response = array(
        'status' => true,
        'data' => $result
      );
    } elseif ($this->input->post('mockup_id')) {
      $mockup = $this->mockup_model->get($this->input->post('mockup_id'));
      if ($mockup) {
        $data = array(
          'user_id' => $this->session->userdata('logged_in')['id'],
          'message' => $this->input->post('msg'),
          'mockup_id' => $mockup->id,
          'status' =>1,
          'created' => current_date(),
          'updated' => current_date()
        );
        $this->comment_model->insert($data);
        $this->mockup_model->update($mockup->id, array('discussion_count'=>$mockup->discussion_count+1,  'updated_on' => current_date()));
        $response = array(
          'status' => true,
          'id'=>$mockup->id,
          'discussion_count' => $mockup->discussion_count+1
        );
      }
    }
    echo json_encode($response);
    exit;
  }


	function reply_view($id){


		$this->load->model('comment_model');
	    $this->data['replies'] = $this->comment_model->get_reply_with_details($id);
	    //echo $this->db->last_query();die();
	    $this->data['mockup'] = $this->mockup_model->get($id);
	    $this->data['user_id'] = $this->session->userdata('logged_in')['id'];
	    $this->load->view('reply', $this->data);
  	}

	 function reply_delete($id = null, $mockup_id = null){


	 	$this->load->model('comment_model');	
	    $response = array('status' => false);
	    $discussion_count =0;
	    if ($id) {
	      $data = array('deleted' => 1);
	      $this->comment_model->update($id, $data);
	      if($mockup_id){
	        $review = $this->mockup_model->get($mockup_id);
	        if ($review) {
	          $discussion_count = $review->discussion_count-1;
	          if($discussion_count < 0){
	            $discussion_count = 0;
	          }
	          $this->mockup_model->update($review->id, array('discussion_count'=>$discussion_count));
	        }
	      }else{
	        $this->mockup_model->update_by(array('mockup_id' => $id), $data);
	      }
	      $response = array(
	        'status' => true,
	        'id' => $id,
	        'mockup_id' => $mockup_id,
	        'discussion_count' => $discussion_count
	      );
	    }
	    echo json_encode($response);
	    exit;
	  }

	public function resizeImage($filename){

      $source_path = 'assets/uploads/' . $filename;
      $target_path = 'assets/uploads/thumbnail/';
      $config_manip = array(
          'image_library' => 'gd2',
          'source_image' => $source_path,
          'new_image' => $target_path,
          'maintain_ratio' => TRUE,
          'create_thumb' => TRUE,
          'thumb_marker' => '_thumb',
          'width' => 200,
          'height' => 200
      );

      $this->load->library('image_lib', $config_manip);
      if (!$this->image_lib->resize()) {
          echo $this->image_lib->display_errors();
      }
      $this->image_lib->clear();
   }

   public function resize_image($image_data){
    $this->load->library('image_lib');
    $w = $image_data['image_width']; // original image's width
    $h = $image_data['image_height']; // original images's height

    $n_w = 200; // destination image's width
    $n_h = 100; // destination image's height

    $source_ratio = $w / $h;
    $new_ratio = $n_w / $n_h;
    if($source_ratio != $new_ratio){

        $config['image_library'] = 'gd2';
        $config['source_image'] = './assets/uploads/'.$image_data['file_name'];
        //$config['new_image'] = './assets/uploads/test/'.$image_data['file_name'];
        $config['maintain_ratio'] = FALSE;
        if($new_ratio > $source_ratio || (($new_ratio == 1) && ($source_ratio < 1))){
            $config['width'] = $w;
            $config['height'] = round($w/$new_ratio);
            $config['y_axis'] = round(($h - $config['height'])/2);
            $config['x_axis'] = 0;

        } else {

            $config['width'] = round($h * $new_ratio);
            $config['height'] = $h;
            $size_config['x_axis'] = round(($w - $config['width'])/2);
            $size_config['y_axis'] = 0;

        }

        $this->image_lib->initialize($config);
        $this->image_lib->crop();
        $this->image_lib->clear();
    }
    $config['image_library'] = 'gd2';
    $config['source_image'] = './assets/uploads/'.$image_data['file_name'];
    $config['new_image'] = './assets/uploads/newresize/'.$image_data['file_name'];
    $config['maintain_ratio'] = TRUE;
    $config['width'] = $n_w;
    $config['height'] = $n_h;
    $this->image_lib->initialize($config);

    // if (!$this->image_lib->resize()){
    //     echo $this->image_lib->display_errors();

    // } else {
    //     echo "done";

    // }
}
	public function delete($id){
		$result = $this->mockup_model->delete('mockup',$id);
		redirect('home');
	}
	public function logout(){
		$this->session->unset_userdata('logged_in');
		redirect('home');
	}
	public function phpinfo(){
		phpinfo();
	}

	public function test_upload(){	

		 if (isset($_FILES['userfile'])){

			 	$config['upload_path']          = './assets/uploads/'; // /mnt/digital_marketing/public_html/
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 1000;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        print_r($error); exit;
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());

                       echo "success"; exit;
                }
		}

		echo '<html>
				<head>
				<title>Upload Form</title>
				</head>
				<body>
				<form action="" method="post" enctype="multipart/form-data">
				<input type="file" name="userfile" size="20" />
				<br /><br />

				<input type="submit" value="upload" />

				</form>

				</body>
				</html>'; exit;

	}
}
