<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mockup_model extends CI_Model{
    function __construct() {
        $this->tableName = 'mockup';
    }

    public function get($primary_key){
        return $this->db->get_where($this->tableName, array("id" => $primary_key))->row();
    }
    public function get_by($params, $tb_name = null){
        return $this->db->get_where((!empty($tb_name)) ? $tb_name : $this->tableName, $params)->row();
    }
    public function get_mockup_detail($id){
        return $this->db->select('u.name,m.*')
                ->from($this->tableName.' m')
                ->join('users u','u.id = m.user_id')
                ->where('m.id',$id)
                ->get()->row();
    }

    public function get_all($params,$tb_name){

        $this->db->where($params);
        return $this->db->get($tb_name);
    }

    
    public function getRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->tableName);

        if(array_key_exists("user_id",$params)){
            $this->db->where(array('user_id'=>$params['user_id']));
        }
        if(array_key_exists("project_id",$params)){
            $this->db->where('project_id',$params['project_id']);
        }
        if(array_key_exists("status",$params)){
            $this->db->where('status',$params['status']);
        }
        if(array_key_exists("search",$params)){
            $this->db->or_like('project_title',$params['search']);
            $this->db->or_like('solution',$params['search']);
            $this->db->or_like('tool',$params['search']);
            $this->db->or_like('subject',$params['search']);
        }
        $this->db->order_by('created_on','desc');
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit']);
        }
        
        $query = $this->db->get();
         
        return ($query->num_rows() > 0)? $query->result():FALSE;
    }

    public function insert($data = array()){
        $query = $this->db->get_where($this->tableName,array('project_title'=>$data['project_title'],'user_id'=>$data['user_id']));
        if($query->num_rows() >0){
            $query = $this->db->where(array('id'=>$data['id']))->update($this->tableName,array('mockup'=>$data['mockup']));
        }else{
            $query = $this->db->insert( $this->tableName,$data);
        }
        
        return $query?true:false;
    }

    public function update($primary_key, $data){
        $query = $this->db->where(array('id'=>$primary_key))->update($this->tableName,$data);
        return $query?true:false;
    }


    function get_by_field($field, $value, $tb_name) {
        $this->db->where($field, $value);
        return $this->db->get($tb_name);
    }
    function insert_det($det, $tb_name){
        $this->db->insert($tb_name, $det);
        return $this->db->insert_id();
    }
    function update_det($det, $value, $tb_name) {
        $this->db->where('id', $value);
        $this->db->update($tb_name, $det);
    }
    function delete($tb_name,$id) {
       $this->db->delete($tb_name, array('id' => $id)); 
       return true;
    }

}