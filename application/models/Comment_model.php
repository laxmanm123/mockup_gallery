<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class comment_model extends CI_Model{
    function __construct() {
        $this->tableName = 'comment';
    }

    public function get($primary_key){
        return $this->db->get_where($this->tableName, array("id" => $primary_key))->row();
    }

    public function insert($data = array()){
        $query = $this->db->insert( $this->tableName,$data);
        return $query?true:false;
    }

    public function update($primary_key, $data){
        $query = $this->db->where(array('id'=>$primary_key))->update($this->tableName,$data);
        return $query?true:false;
    }

    public function get_reply_with_details($id){
        $query = $this->db->query("select c.*,u.name from comment c inner join users u on u.id = c.user_id where mockup_id = $id and c.status = true and deleted = false order by id desc");
        if ($query->num_rows() > 0) {
          return $query->result();
        } else {
          return false;
        }
    }
}

?>