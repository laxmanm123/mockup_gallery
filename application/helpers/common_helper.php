<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


function current_date(){
	return date("Y-m-d H:i:s");
}
function dashboard_date_format($date)
{
  return date("H:i d-M-y", strtotime($date));
}
function power_user_list(){
	return array('subramani@eidesign.net', 'somab@eidesign.net', 'vishnur@eidesign.net', 'sinchur@eidesign.net','laxmanm@eidesign.net');
}
function highlightKeywords($text, $keyword) {
	if($keyword){
		$wordsAry = explode(" ", $keyword);
		$wordsCount = count($wordsAry);
		
		for($i=0;$i<$wordsCount;$i++) {
			$highlighted_text = "<span class='bg-warning' style='font-weight:bold;'>$wordsAry[$i]</span>";
			$text = str_ireplace($wordsAry[$i], $highlighted_text, $text);
		}
	}
	return $text;
}
/* ------------------------------ Session	+ Site ------------------------------ */
function get_asset($type, $file, $path = 'base', $version = TRUE)
{
  $out = base_url() . 'assets/';
  $out .= $type . '/';
  if ($path != 'base')
    $out .= $path . '/';

  $out .= $file;

  if ($version)
    $out .= '?v=' . get_version();

  return $out;
}
function get_version()
{
  $CI =& get_instance();
  return $CI->config->item('version');
}